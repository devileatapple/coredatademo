//
//  CoreDataManager.h
//  VeryTele
//
//  Created by ZhiQiangMi on 14/10/23.
//  Copyright (c) 2014年 ZhiQiangMi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
//#import "TKAddressBook.h"
//#import "ContactsData.h"
//定义模型类型
//typedef enum
//{
//    LocationThemeCoreType=0,
//    RecommendedThemeCoreType,
//    LeaderboardThemeCoreType,
//    AllThemeCoreType,
//    ShareDataCoreType,
//    UserDataCoreType,
//    LocationPhoneNumberType,
//    AddressBookType,
//    VTUserNumberType,
//    ContactsType
//}CoreDataType;

@interface CoreDataManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext* managedObjectContext;

// 初始化
+ (CoreDataManager*)sharedManager;

// 数据操作
#pragma mark - 增
- (BOOL)insertNewData:(id)_requestResult entityName:(NSString *)_entityName entityType:(int)_entityType;
#pragma mark - 删
-(void)deleteDataWithEntiyName:(NSString *)entiyName Predicate:(NSString *)predicate;

#pragma mark - 查
- (NSMutableArray*)searchDataWithEntityName:(NSString *)_entityName Predicate:(NSString *)predicateStr sortkey:(NSString *)_sortKey;

#pragma mark - 更新
- (NSManagedObject*)insertNewData:(id)_requestResult entityName:(NSString *)_entityName entityType:(int)_entityType isDeleteData:(BOOL)_isDeleteData;
//-(void)insertDataToContactsWithTkAddressBook:(TKAddressBook * )book;
//-(void)deleteContactsWith:(ContactsData *)data;
//-(NSArray *)fetchData;
#pragma mark - 改变首字母为拼音
-(NSString *)TransFromFirstStrWith:(NSString *)str;
#pragma mark - 数据保存
-(void)saveCoreData;


@end
