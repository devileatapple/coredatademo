//
//  ViewController.m
//  coreDataDemo
//
//  Created by Brain on 15/9/8.
//  Copyright (c) 2015年 Brain. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Person.h"
#import "IDCard.h"
#import "CoreDataManager.h"
@interface ViewController ()
{
    NSManagedObjectContext *manger;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    manger = [CoreDataManager sharedManager].managedObjectContext;
//    manger=[AppDelegate sharedInstance].managedObjectContext;
    //创建并插入一个Person对象
//    [self addPerson];

    //查询Person对象
//    [self searchPerson];
    //删除Person对象
//    [self deletePerson];
    //查询Person对象
//    [self searchPerson];
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)addPerson
{
    Person *p = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
    p.name=@"Amy";
    p.age= [NSNumber numberWithInt:21];
    [[CoreDataManager sharedManager] saveCoreData];
}
-(void)searchPerson
{
    //查询结果
    NSMutableArray *arr = [[CoreDataManager sharedManager] searchDataWithEntityName:@"Person"  Predicate:nil sortkey:nil];
    //修改
//    Person *p = [arr lastObject];
//    [p setValue:@"Green" forKey:@"name"];
//    [p setValue:[NSNumber numberWithInt:18] forKey:@"age"];
//    [p setValue:@"xxxx" forKey:@"idnumber"];
//    [[CoreDataManager sharedManager]saveCoreData];
    //打印
    for (Person *p in arr)
    {
        NSLog(@"name:%@,age:%@",p.name,p.age);
    }

}
-(void)deletePerson
{
    [[CoreDataManager sharedManager] deleteDataWithEntiyName:@"Person" Predicate:@"name='Green'"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
