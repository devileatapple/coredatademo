//
//  personDetailsVC.m
//  coreDataDemo
//
//  Created by Brain on 15/12/3.
//  Copyright © 2015年 Brain. All rights reserved.
//

#import "personDetailsVC.h"
#import <CoreData/CoreData.h>
#import "Person.h"
#import "IDCard.h"
#import "IDCard+CoreDataProperties.h"
#import "CoreDataManager.h"
@interface personDetailsVC ()
@property (weak, nonatomic) IBOutlet UITextField *nameTxf;
@property (weak, nonatomic) IBOutlet UITextField *ageTxf;
@property (weak, nonatomic) IBOutlet UITextField *IDNumberTxf;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property(assign,nonatomic)BOOL flagDismiss;//用于判断是否要Dismiss视图

@end

@implementation personDetailsVC
@synthesize person;
- (void)viewDidLoad
{
    [super viewDidLoad];
    _nameTxf.text=person.name;
    _ageTxf.text=person.age?[NSString stringWithFormat:@"%@",person.age]:@"";
    IDCard *card =(IDCard *)person.card;
    _IDNumberTxf.text=card.cardNumber;
    NSLog(@"idnumber=%@",person.card);
    if (!card)
    {
        _IDNumberTxf.text=@"";
    }
    // Do any additional setup after loading the view.
}

- (IBAction)btnClick:(id)sender
{
    _flagDismiss=NO;
    UIButton * btn=(UIButton *)sender;
    /*
     可以使用 switch btn.tag;
     */
    
    if (btn==_addBtn)
    {
        [self addPerson];
    }
    if (btn==_deleteBtn)
    {
        [self deletePerson];
    }
    if (btn==_changeBtn)
    {
        [self changePerson];
    }
    if (btn==_searchBtn)
    {
        [self searchPerson];
    }
    if (btn==_submitBtn)
    {
        if (!person)
        {
            person = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
        }
        NSEntityDescription *card_entity = [NSEntityDescription entityForName:@"IDCard" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
        person.name=_nameTxf.text;
        person.age=_ageTxf.text.length?[NSNumber numberWithInt:[_ageTxf.text intValue]]:nil;
        if (_nameTxf.text.length>0)
        {
            IDCard *idnumber=[[IDCard alloc]initWithEntity:card_entity insertIntoManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
            idnumber.cardNumber=_IDNumberTxf.text;
            person.card=idnumber;
        }
        
        [[CoreDataManager sharedManager] saveCoreData];
        _flagDismiss=YES;
    }
    if (btn==_cancelBtn)
    {
        _flagDismiss=YES;
    }
    if (_flagDismiss)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)addPerson
{
    if (_nameTxf.text.length==0 || _ageTxf.text.length==0)
    {
        NSLog(@"姓名或者岁数不可为空");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"不能为空" message:@"姓名或者岁数不可为空" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    Person *p = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
    p.name=_nameTxf.text;
    p.age= [NSNumber numberWithInt:[_ageTxf.text intValue]];
    //身份信息如果不为空的话
    if (_IDNumberTxf.text.length>0)
    {
        IDCard *idnumber=[NSEntityDescription insertNewObjectForEntityForName:@"IDCard" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
        idnumber.cardNumber=_IDNumberTxf.text;
        idnumber.person=p;
        p.card=idnumber;
    }
    [[CoreDataManager sharedManager] saveCoreData];
    _flagDismiss=YES;
}
-(void)deletePerson
{
    NSString * PredicateStr;
    if (_nameTxf.text.length>0)
    {
        PredicateStr = [PredicateStr stringByAppendingString:[NSString stringWithFormat:@"name='%@' ",_nameTxf.text]];
    }
    if (_ageTxf.text.length>0)
    {
        PredicateStr = [PredicateStr stringByAppendingString:[NSString stringWithFormat:@"age==%@ ",_ageTxf.text]];
    }
    //通过年龄以及名字搜索出对应的person
    NSMutableArray *personArr=[[CoreDataManager sharedManager]searchDataWithEntityName:@"Person" Predicate:PredicateStr sortkey:nil];
    //遍历person
    for (Person *person1 in personArr)
    {
        IDCard *card =(IDCard *)person1.card;
        //如果IDCard不为空的时候做精确删除。
        if (_IDNumberTxf.text.length>0)
        {
            if ([card.cardNumber isEqualToString:_IDNumberTxf.text])
            {
                [[CoreDataManager sharedManager].managedObjectContext deleteObject:person1];
                [[CoreDataManager sharedManager].managedObjectContext deleteObject:card];
            }
        }
        else
        {
            //IDNumber为空的时候把所有符合条件的person全部删除
            [[CoreDataManager sharedManager].managedObjectContext deleteObject:person1];
        }
        
    }
    
    [[CoreDataManager sharedManager].managedObjectContext save:nil];
    _flagDismiss=YES;


}
-(void)changePerson
{
//    Person *p = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
    person.name=_nameTxf.text;
    person.age= [NSNumber numberWithInt:[_ageTxf.text intValue]];
    if (_IDNumberTxf.text.length>0)
    {
        NSEntityDescription *card_entity = [NSEntityDescription entityForName:@"IDCard" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
        IDCard *idnumber=[[IDCard alloc]initWithEntity:card_entity insertIntoManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
        idnumber.cardNumber=_IDNumberTxf.text;
        person.card = idnumber;
        idnumber.person=person;
    }
    [[CoreDataManager sharedManager] saveCoreData];
}
-(void)searchPerson
{
    
    NSString *PredicateStr;
    if(_IDNumberTxf.text.length>0)
    {
        NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"IDCard"];
        request.predicate=[NSPredicate predicateWithFormat:@"cardNumber = %@",_IDNumberTxf.text];
        IDCard *card1=[[[CoreDataManager sharedManager].managedObjectContext executeFetchRequest:request error:nil]lastObject];
        NSLog(@"card.Number=%@",card1.cardNumber);
        Person *p=(Person *)card1.person;
        _nameTxf.text=p.name;
        _ageTxf.text=p?[NSString stringWithFormat:@"%@",p.age]:@"";
        return;
    }
    if (_nameTxf.text.length>0)
    {
        PredicateStr= [PredicateStr stringByAppendingString:[NSString stringWithFormat:@"name='%@' ",_nameTxf.text]] ;
    }
    else if(_ageTxf.text.length>0)
    {
        PredicateStr= [PredicateStr stringByAppendingString:[NSString stringWithFormat:@"age==%@ ",_ageTxf.text]] ;
    }
    Person *p =  [[[CoreDataManager sharedManager] searchDataWithEntityName:@"Person" Predicate:PredicateStr sortkey:nil] lastObject];
    IDCard *card=(IDCard *)p.card;
    if(p)
    {
        _nameTxf.text=p.name;
        _ageTxf.text=[NSString stringWithFormat:@"%@",p.age];
        _IDNumberTxf.text=card.cardNumber?card.cardNumber:@"";
    }
    else
    {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"搜索结果" message:@"未搜索到结果，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
        {
            NSLog(@"确定");
        }];
        UIAlertAction *action1=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
        {
            NSLog(@"取消");
        }];
        [alert addAction:action];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
