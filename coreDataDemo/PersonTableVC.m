//
//  PersonTableVC.m
//  coreDataDemo
//
//  Created by Brain on 15/12/3.
//  Copyright © 2015年 Brain. All rights reserved.
//

#import "PersonTableVC.h"
#import "personDetailsVC.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Person.h"
#import "IDCard.h"
#import "CoreDataManager.h"
@interface PersonTableVC ()

@property (strong, nonatomic) IBOutlet UITableView *table;

@end

@implementation PersonTableVC
@synthesize personArr;
- (void)viewDidLoad {
    [super viewDidLoad];
    personArr=[[NSMutableArray alloc]init];
//    [self addPerson];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshTableData];
}
- (IBAction)addPersonClick:(id)sender
{
    personDetailsVC *personDetails= [[UIStoryboard storyboardWithName:@"AppStory" bundle:nil] instantiateViewControllerWithIdentifier:@"personDetailsVC"];;
    [self.navigationController presentViewController:personDetails animated:YES completion:nil];
}
-(void)addPerson
{
    Person *p = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
    p.name=@"Amy";
    p.age= [NSNumber numberWithInt:21];
    NSEntityDescription *card_entity = [NSEntityDescription entityForName:@"IDCard" inManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
    IDCard *idnumber=[[IDCard alloc]initWithEntity:card_entity insertIntoManagedObjectContext:[CoreDataManager sharedManager].managedObjectContext];
    idnumber.cardNumber=[NSString stringWithFormat:@"%@_%i",p.name,1];
    p.card=idnumber;
    [[CoreDataManager sharedManager] saveCoreData];
}

-(void)searchPerson
{
    //查询结果
    NSMutableArray *arr = [[CoreDataManager sharedManager] searchDataWithEntityName:@"Person"  Predicate:nil sortkey:nil];
    //打印
    for (Person *p in arr)
    {
        NSLog(@"name:%@,age:%@",p.name,p.age);
    }
    
}


-(void)refreshTableData
{
    [personArr removeAllObjects];
    personArr=[[CoreDataManager sharedManager] searchDataWithEntityName:@"Person"  Predicate:nil sortkey:nil];
    for (Person *p in personArr)
    {
        NSLog(@"name:%@,age:%@",p.name,p.age);
    }
    [self.table reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return personArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Person *p=personArr[indexPath.row];
    IDCard *card=(IDCard *)p.card;
    if (!card)
    {
        cell.textLabel.text=[NSString stringWithFormat:@"name:%@   age:%@  idCard:%@",p.name,p.age,@""];
    }
    else
    {
        cell.textLabel.text=[NSString stringWithFormat:@"name:%@   age:%@  idCard:%@",p.name,p.age,card.cardNumber];
    }
    // Configure the cell...
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    personDetailsVC *personDetails= [[UIStoryboard storyboardWithName:@"AppStory" bundle:nil] instantiateViewControllerWithIdentifier:@"personDetailsVC"];;
    Person *p=personArr[indexPath.row];
    personDetails.person=p;
    [self.navigationController presentViewController:personDetails animated:YES completion:nil];

}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
