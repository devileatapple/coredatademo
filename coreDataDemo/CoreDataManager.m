//
//  CoreDataManager.m
//  VeryTele
//
//  Created by ZhiQiangMi on 14/10/23.
//  Copyright (c) 2014年 ZhiQiangMi. All rights reserved.
//

#import "CoreDataManager.h"
//#import "Person.h"
//#import "IDCard.h"
//#import "LocationThemeData.h"
//#import "LeaderboardThemeData.h"
//#import "AllThemeData.h"
//#import "RecommendedThemeData.h"
//#import "ShareData.h"
//#import "UserData.h"
//#import "LocationPhoneNumbersData.h"
//#import "TKAddressBook.h"
//#import "AddressBookData.h"
//#import "VTUserData.h"
@implementation CoreDataManager
{
    NSManagedObjectContext* _managedObjectContext;
    BOOL isInit;
}

#pragma mark --- 初期化 ---
static CoreDataManager* _sharedInstance = nil;

+ (CoreDataManager*)sharedManager
{
    // 单例
    if (!_sharedInstance) {
        _sharedInstance = [[CoreDataManager alloc] init];
    }
    return _sharedInstance;
}

#pragma mark --- coredata上下文 ---
- (NSManagedObjectContext*)managedObjectContext
{
    NSError* error = nil;
    isInit = NO;
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    // 管理对象模型声明
    NSManagedObjectModel* managedObjectModel;
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    // store URL声明
    NSArray* paths;
    NSString* path = nil;
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"paths : %@", paths);
    if ([paths count] > 0) {
        path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:@"App"];
        path = [path stringByAppendingPathComponent:@"App.db"];
    }
    
    if (!path) {
        return nil;
    }
    
    
    // 目录声明
    NSString* dirPath;
    NSFileManager* fileMgr;
    dirPath = [path stringByDeletingLastPathComponent];
    fileMgr = [NSFileManager defaultManager];
    if (![fileMgr fileExistsAtPath:dirPath]) {
        if (![fileMgr createDirectoryAtPath:dirPath
                withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Failed to create directory at path %@, error %@",
                  dirPath, [error localizedDescription]);
        }
        isInit = YES;
    }
    
    // store URL 声明
    NSURL* url = nil;
    url = [NSURL fileURLWithPath:path];
    
    // 持久化 声明
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSPersistentStoreCoordinator* persistentStoreCoordinator;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: managedObjectModel];
    NSPersistentStore* persistentStore;
    persistentStore = [persistentStoreCoordinator
                       addPersistentStoreWithType:NSSQLiteStoreType
                       configuration:nil URL:url options:options error:&error];
    //                       configuration:nil URL:url options:nil error:&error];
    if (!persistentStore && error) {
        NSLog(@"Failed to create add persistent store, %@", [error localizedDescription]);
    }
    
    // 管理对象模型声明
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    
    // 上下文设定持久化助理
    [_managedObjectContext setPersistentStoreCoordinator:persistentStoreCoordinator];
    return _managedObjectContext;
}
#pragma mark --- 插入操作
- (BOOL)insertNewData:(id)_requestResult entityName:(NSString *)_entityName entityType:(int)_entityType
{
    //通过entityName创建一个插入对象
    NSManagedObject *model = [NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
    switch (_entityType)
    {
        case 0:
        {
//            Person *p=(Person *)_requestResult;
//            NSLog(@"name:%@,age=%@",p.name,p.age);
//            [model setValue:p.name forKey:@"name"];
//            [model setValue:p.age forKey:@"age"];
//            [model setValue:p.idnumber forKey:@"idnumber"];
        }
            break;
            
        default:
            break;
    }
    // 保存
    NSError*    error;
    BOOL flag =[self.managedObjectContext save:&error];
    if (!flag) {
        // エラー
        NSLog(@"Save Error, %@", error);
    }
    return flag;
}
//根据entiyName删除对应的对象
#pragma mark --- 删除操作
-(void)deleteDataWithEntiyName:(NSString *)entiyName Predicate:(NSString *)predicateStr
{
    NSEntityDescription *description = [NSEntityDescription entityForName:entiyName inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setIncludesPropertyValues:NO];
    [request setEntity:description];
    if (predicateStr!=nil) {
        NSPredicate* predicate=[NSPredicate predicateWithFormat:predicateStr];
        [request setPredicate:predicate];
    }
    NSError *error = nil;
    NSArray *datas = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (!error && datas && [datas count])
    {
        for (NSManagedObject *obj in datas)
        {
            [self.managedObjectContext deleteObject:obj];
        }
        if (![self.managedObjectContext save:&error])
        {
            NSLog(@"error:%@",error);
        }
    }
}
#pragma mark --- 修改操作

#pragma mark --- 查询操作 ---
- (NSMutableArray*)searchDataWithEntityName:(NSString *)_entityName Predicate:(NSString *)predicateStr sortkey:(NSString *)_sortKey
{
    // 上下文
    NSManagedObjectContext* context;
    context = self.managedObjectContext;
    
    // 查询请求设置
    NSFetchRequest*         request;
    NSEntityDescription*    entity;
    //排序
    NSSortDescriptor*       sortDescriptor;
    request = [[NSFetchRequest alloc] init];
    //根据_entityName获取对象模型entity
    entity = [NSEntityDescription entityForName:_entityName inManagedObjectContext:context];
    //设置请求对象模型
    [request setEntity:entity];
    
    [request setReturnsObjectsAsFaults:NO];
    //如果搜索关键字_sortKey不为空，设置搜索条件
    if (predicateStr!=nil)
    {
        NSPredicate* predicate=[NSPredicate predicateWithFormat:predicateStr];
        [request setPredicate:predicate];
    }
    //排序
    if (_sortKey!=nil) {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:_sortKey ascending:YES];
        [request setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    }
    
    // 取得查询结果
    NSMutableArray    *result;
    NSError*    error = nil;
    
    @try{
        BOOL isSaveSuccess = [self.managedObjectContext save:&error];
        if (!isSaveSuccess)
        {
            NSLog(@"Error: %@,%@",error,[error userInfo]);
        }else {
            
            NSLog(@"%@  Save successful!",_entityName);
            result = [[context executeFetchRequest:request error:&error] mutableCopy];

        }
    }
    @catch(NSException *exception) {
        NSLog(@"exception:%@", exception);
    }
    @finally {
        
    }
    return result;
    
}














//插入新数据_requestResult，模型名_entityName，通过传入的entityType对对应的模型做操作，是否删除旧数据isDeleteData
- (NSManagedObject*)insertNewData:(id)_requestResult entityName:(NSString *)_entityName entityType:(int)_entityType isDeleteData:(BOOL)_isDeleteData
{
    //NSFetchRequest通过_entityName获取对象模型
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:_entityName inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    fetchRequest = nil;
    
    if (_isDeleteData) {
        for (NSManagedObject *managedObject in items) {
            [self.managedObjectContext deleteObject:managedObject];
        }
    }
    
//    switch (_entityType) {
//        case LocationThemeCoreType:{
//            LocationThemeData *locationThemeData = (LocationThemeData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//            [locationThemeData setThemeContent:[_requestResult objectForKey:@"themeContent"]];
//            [locationThemeData setThemeHits:[NSString stringWithFormat:@"%@",[_requestResult objectForKey:@"themeHits"]]];
//            [locationThemeData setThemeId:[NSString stringWithFormat:@"%@",[_requestResult objectForKey:@"themeId"]]];
//            [locationThemeData setThemeName:[_requestResult objectForKey:@"themeName"]];
//            [locationThemeData setThemePicUrl:[_requestResult objectForKey:@"themePicUrl"]];
//            [locationThemeData setThemeType:[NSString stringWithFormat:@"%@",[_requestResult objectForKey:@"themeType"]]];
//            [locationThemeData setThemeZipUrl:[_requestResult objectForKey:@"themeZipUrl"]];
//        }
//            break;
//        case RecommendedThemeCoreType:{
//             for(NSDictionary *data in _requestResult){
//            RecommendedThemeData *recommendedThemeData = (RecommendedThemeData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//            [recommendedThemeData setThemeContent:[data objectForKey:@"themeContent"]];
//            [recommendedThemeData setThemeHits:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeHits"]]];
//            [recommendedThemeData setThemeId:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeId"]]];
//            [recommendedThemeData setThemeName:[data objectForKey:@"themeName"]];
//            [recommendedThemeData setThemePicUrl:[data objectForKey:@"themePicUrl"]];
//            [recommendedThemeData setThemeType:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeType"]]];
//            [recommendedThemeData setThemeZipUrl:[data objectForKey:@"themeZipUrl"]];
//             }
//        }
//            break;
//        case LeaderboardThemeCoreType:{
//            for(NSDictionary *data in _requestResult){
//            LeaderboardThemeData *leaderboardThemeData = (LeaderboardThemeData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//            [leaderboardThemeData setThemeContent:[data objectForKey:@"themeContent"]];
//            [leaderboardThemeData setThemeHits:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeHits"]]];
//            [leaderboardThemeData setThemeId:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeId"]]];
//            [leaderboardThemeData setThemeName:[data objectForKey:@"themeName"]];
//            [leaderboardThemeData setThemePicUrl:[data objectForKey:@"themePicUrl"]];
//            [leaderboardThemeData setThemeType:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeType"]]];
//            [leaderboardThemeData setThemeZipUrl:[data objectForKey:@"themeZipUrl"]];
//            }
//        }
//            break;
//        case AllThemeCoreType:{
//            for(NSDictionary *data in _requestResult){
//            AllThemeData *allThemeData = (AllThemeData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//            [allThemeData setThemeContent:[data objectForKey:@"themeContent"]];
//            [allThemeData setThemeHits:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeHits"]]];
//            [allThemeData setThemeId:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeId"]]];
//            [allThemeData setThemeName:[data objectForKey:@"themeName"]];
//            [allThemeData setThemePicUrl:[data objectForKey:@"themePicUrl"]];
//            [allThemeData setThemeType:[NSString stringWithFormat:@"%@",[data objectForKey:@"themeType"]]];
//            [allThemeData setThemeZipUrl:[data objectForKey:@"themeZipUrl"]];
//            }
//        }
//            break;
//        case ShareDataCoreType:{
//                ShareData *shareData = (ShareData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//            [shareData setTelNo:[_requestResult objectForKey:@"telNo"] ];
//            [shareData setLastTime:[_requestResult objectForKey:@"lastTime"]];
//            [shareData setResponse:[_requestResult objectForKey:@"response"]];
//            [shareData setShareUrl:[_requestResult objectForKey:@"shareUrl"]];
//        }
//            break;
//        case UserDataCoreType:{
//            UserData *userData = (UserData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//            [userData setRemainTime:[_requestResult objectForKey:@"remainTime"]];
//            [userData setSaveMoney:[_requestResult objectForKey:@"saveMoney"]];
//            [userData setSuggestTeleNo:[_requestResult objectForKey:@"suggestTeleNo"]];
//            [userData setTeleNo:[_requestResult objectForKey:@"teleNo"]];
//            [userData setUserState:[_requestResult objectForKey:@"userState"]];
//            [userData setUserid:[_requestResult objectForKey:@"userid"]];
//            [userData setUuid:[_requestResult objectForKey:@"uuid"]];
//            [userData setSipDomain:[_requestResult objectForKey:@"sipDomain"]];
//            [userData setSipIP:[_requestResult objectForKey:@"sipIP"]];
//            [userData setSipPassword:[_requestResult objectForKey:@"sipPassword"]];
            
//        }
//            break;
//        case AddressBookType:{
//            for(TKAddressBook *data in _requestResult){
//                AddressBookData *addressBookData = (AddressBookData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//                [addressBookData setIsUser:data.isUser];
//                [addressBookData setName:data.name];
//                [addressBookData setTel:data.tel];
//                if (data.thumbnail!=nil) {
//                    [addressBookData setThumbnail:data.thumbnail];
//                }
//            }
            
//        }
//            break;
//        case VTUserNumberType:
//        {
//            for(int i=0;i< [_requestResult count];i++)
//            {
//                NSString *VTUSerNumber=[_requestResult objectAtIndex:i];
//                NSLog(@"VTUSerNumber=%@",VTUSerNumber);
//                VTUserData *VTUSerNumbers = (VTUserData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//                [VTUSerNumbers setTel:VTUSerNumber];
//            }
//        }
//            break;
//            case ContactsType:
//        {
//            NSMutableArray * arr=[self createRandomRecoardIDWithCount:[_requestResult count]];
//            
//            for (int i =0; i<[_requestResult count]; i++) {
//                NSDictionary * dict=[_requestResult objectAtIndex:i];
////                NSLog(@"dict=%@",dict);
//                //插入对象
//               ContactsData *contacts = (ContactsData *)[NSEntityDescription insertNewObjectForEntityForName:_entityName inManagedObjectContext:self.managedObjectContext];
//                
//                NSString *friend=[NSString stringWithFormat:@"%@",[dict objectForKey:@"friend"]];
//                NSString *friendName=[NSString stringWithFormat:@"%@",[dict objectForKey:@"friendName"]];
//                if (friend.length>0) {
//                    contacts.tel=friend;
//                    contacts.name=friendName;
////                    long long tel=[contacts.tel longLongValue];
//                    //NSNumber *ID=[NSNumber numberWithLongLong:tel];
//                    contacts.recordID=[[arr objectAtIndex:i] integerValue];
//                    contacts.isUser=YES;
//                    contacts.sectionName =[self TransFromFirstStrWith:contacts.name];
//                }
//            }
//        }
//        default:
//            break;
//    }
    
    NSError *_error;
    //托管对象准备好后，调用托管对象上下文的save方法将数据写入数据库
    @try{
        BOOL isSaveSuccess = [self.managedObjectContext save:&_error];
        if (!isSaveSuccess) {
            NSLog(@"Error: %@,%@",_error,[_error userInfo]);
        }else {
            
            NSLog(@"%@  Save successful!",_entityName);
        }
    }
    @catch(NSException *exception) {
        NSLog(@"exception:%@", exception);
    }
    @finally {
        
    }
    
    
    
    return nil;
    
}




//-(NSArray *)fetchData {
//    NSFetchRequest * request = [[NSFetchRequest alloc]initWithEntityName:@"ContactsData"];
//    NSArray * arr = [self.managedObjectContext executeFetchRequest:request error:nil];
//    return  arr;
//}
//-(void)insertDataToContactsWithTkAddressBook:(TKAddressBook * )book
//{
//    NSArray * dataArr=[self fetchData];
//    ContactsData * data = [NSEntityDescription insertNewObjectForEntityForName:@"ContactsData" inManagedObjectContext:self.managedObjectContext];
//    
//    data.name = book.name;
//    data.tel = book.tel;
////    NSInteger tel=[book.tel integerValue];
//   // NSNumber *ID=[NSNumber numberWithLongLong:tel];
//    
//    data.recordID = dataArr.count +1;
//    data.isUser=YES;
//    
//    data.sectionName =[self TransFromFirstStrWith:book.name];
//    //[self TransFromFirstStrWith:book.name];
//    //        NSLog(@"***********************");
//    //        NSLog(@"%@,%@",book.name,[self TransFromFirstStrWith:book.name]);
//    //data.thumbnail = book.thumbnail;
//    NSError * err =nil;
//    @try{
//        BOOL isSaveSuccess = [self.managedObjectContext save:&err];
//        if (!isSaveSuccess) {
//            NSLog(@"Error: %@,%@",err,[err userInfo]);
//        }else {
//            //
//            //                NSLog(@"Save successful!");
//        }
//        
//    }
//    @catch(NSException *exception) {
//        NSLog(@"exception:%@", exception);
//    }
//    @finally {
//        
//    }
//}
//-(void)deleteContactsWith:(ContactsData *)data{
//    
//    NSFetchRequest * request = [[NSFetchRequest alloc]initWithEntityName:@"ContactsData"];
//    //recordID为查询条件
//    NSPredicate* predicate=[NSPredicate predicateWithFormat:@"recordID==%li",data.recordID];
//    [request setPredicate:predicate];
//    NSArray * arr = [self.managedObjectContext executeFetchRequest:request error:nil];
//    if ([arr containsObject:data]) {
//        [self.managedObjectContext deleteObject:data];
//    }
//    NSError * err =nil;
//    BOOL ret= [self.managedObjectContext save:&err];
//    if (!ret) {
//        NSLog(@"%@",err);
//    }
//}

#pragma mark - 创建随机RecoardID
-(NSMutableArray *)createRandomRecoardIDWithCount:(NSInteger)count{
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    NSInteger i;
//    NSLog(@"count:%ld",(long)count);
    for (i = 0; i < count; i ++) {
        [tempArray addObject:[NSNumber numberWithInteger:i]];
        
    }

    for (i = 0; i < count; i ++) {
       
        int index = arc4random() % (count - i);
        [resultArray addObject:[tempArray objectAtIndex:index]];
//        NSLog(@"index:%d,xx:%@",index,[tempArray objectAtIndex:index]);
        [tempArray removeObjectAtIndex:index];
        
    }
    return resultArray;
}

#pragma mark - 将第一个字符转换为拼音的方法
-(NSString *)TransFromFirstStrWith:(NSString *)str{
    
    NSMutableString * mutableStr=[[NSMutableString alloc]initWithString:str];
    CFStringTransform((__bridge CFMutableStringRef)mutableStr, NULL, kCFStringTransformMandarinLatin, NO) ;
    CFStringTransform((__bridge CFMutableStringRef)mutableStr, NULL, kCFStringTransformStripDiacritics, NO);//转换成拼音
    if(str.length==0)
    {
        str=@"未知用户";
        mutableStr=[NSMutableString stringWithString:@"未知用户"];
    }
    if ([self isPureInt:[str substringToIndex:1]]) {
        return str;
    }
    else
    {
        NSString * first=[[mutableStr substringToIndex:1] uppercaseString];//抽取第一个作为分组名
        return first;
    }
    
}

- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

#pragma mark --- 永続化 ---

- (void)saveCoreData
{
    // 保存
    NSError*    error;
    if (![self.managedObjectContext save:&error]) {
        // エラー
        NSLog(@"Save Error, %@", error);
    }
}


@end
