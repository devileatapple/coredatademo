//
//  PersonTableVC.h
//  coreDataDemo
//
//  Created by Brain on 15/12/3.
//  Copyright © 2015年 Brain. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PersonTableVC : UITableViewController

@property(retain,nonatomic)NSMutableArray * personArr;
@end
