//
//  IDCard.h
//  coreDataDemo
//
//  Created by Brain on 15/12/22.
//  Copyright © 2015年 Brain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Person;

NS_ASSUME_NONNULL_BEGIN

@interface IDCard : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "IDCard+CoreDataProperties.h"
