//
//  IDCard+CoreDataProperties.h
//  coreDataDemo
//
//  Created by Brain on 15/12/22.
//  Copyright © 2015年 Brain. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IDCard.h"

NS_ASSUME_NONNULL_BEGIN

@interface IDCard (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *cardNumber;
@property (nullable, nonatomic, retain) Person *person;

@end

NS_ASSUME_NONNULL_END
