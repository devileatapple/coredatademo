//
//  IDCard+CoreDataProperties.m
//  coreDataDemo
//
//  Created by Brain on 15/12/22.
//  Copyright © 2015年 Brain. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IDCard+CoreDataProperties.h"

@implementation IDCard (CoreDataProperties)

@dynamic cardNumber;
@dynamic person;

@end
